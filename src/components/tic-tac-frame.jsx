import React, { Component } from "react";
import ButtonsSet from "./buttonsSet";
import WinnerBar from "./winnerDisplay";
import "../components/ticTac.css";

class TicTacFrame extends Component {
  state = {
    booleanValue: true,
    alert: "",
    buttons: [
      { key: 1, value: "" },
      { key: 2, value: "" },
      { key: 3, value: "" },
      { key: 4, value: "" },
      { key: 5, value: "" },
      { key: 6, value: "" },
      { key: 7, value: "" },
      { key: 8, value: "" },
      { key: 9, value: "" },
    ],
    valuesToCheck: [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ],
  };

  checkButtonsValues = () => {
    let condition = true;
    this.state.valuesToCheck.forEach((line) => {
      let [a, b, c] = line;
      let buttons = this.state.buttons;
      if (
        buttons[a].value &&
        buttons[a].value === buttons[b].value &&
        buttons[a].value === buttons[c].value
      ) {
        condition = false;
        this.setState({ alert: `winner ${buttons[a].value}` });
      }
    });
    return condition;
  };

  framChanged = (boolean, id) => {
    if (this.checkButtonsValues()) {
      let copyState = this.state.buttons;

      copyState.forEach((button) => {
        this.setState({ booleanValue: !boolean });

        if (button.key === id && button.value.length === 0) {
          if (boolean) button.value = "X";
          else {
            button.value = "O";
          }
        }
      });
      this.setState({ buttons: copyState });
    }
  };

  resetAll = () => {
    let copyOfButtons = [...this.state.buttons];
    copyOfButtons.map((button) => {
      button.value = "";
    })
      this.setState({ buttons: copyOfButtons });
  };

  render() {
    return (
      <div className="frame">
        <button onClick={this.resetAll} type="reset">
          Reset
        </button>
        <WinnerBar onalert={this.state.alert} />
        <ButtonsSet
          handleClick={this.framChanged}
          onButtons={this.state.buttons}
          boolean={this.state.booleanValue}
        />
      </div>
    );
  }
}
export default TicTacFrame;
