import React, { Component } from "react";
import "../components/ticTac.css";

class ButtonsSet extends Component {

  render() {
    return (
      <div className="buttonSet">
        {this.props.onButtons.map((button) => {
          return (
            <button
              onClick={()=> this.props.handleClick(this.props.boolean,button.key)}
              key={button.key}
              className="btn btn-primary m-2"
            >
              {button.value}
            </button>
          );
        })}
      </div>
    );
  }
}
export default ButtonsSet;
