import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import React, { Component } from 'react';
import './App.css'
import TicTacFrame from './components/tic-tac-frame';

function App() {
  return (
    <React.Fragment>
      <TicTacFrame />
    </React.Fragment>
  );
}
export default App